package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.InvalidParameterException;

import org.junit.Test;

import stringcalculator.StringCalculator;

public class StringCalculatorTest {
	
	private StringCalculator stringCalculator = new StringCalculator();
	
	@Test
	public void should_return_zero_for_empty_string() {
		assertEquals(0, stringCalculator.add(""));
		assertEquals(0, stringCalculator.add("0,0"));
		assertEquals(3, stringCalculator.add("1,2"));
		assertEquals(3, stringCalculator.add("1, 2"));
		assertEquals(15, stringCalculator.add("1,2,3,4,5"));
		assertEquals(6, stringCalculator.add("2\n2,2"));
		assertEquals(3, stringCalculator.add("//;\n1;2"));
	}
	
	@Test
	public void should_throw_an_exception_for_negative_numbers() {
		assertThrows(InvalidParameterException.class, ()-> {stringCalculator.add("1, -1");});
	}

	@Test
	public void should_ignore_number_greater_than_1000() {
		assertEquals(2, stringCalculator.add("1001,2"));
	}
	
	@Test
	public void should_accept_any_length_of_delimiter() {
		assertEquals(6, stringCalculator.add("//[***]\n1***2***3"));
	}
	
	@Test
	public void should_accept_multiple_delimiters() {
		assertEquals(6, stringCalculator.add("//[*][%]\n1*2%3"));
		assertEquals(6, stringCalculator.add("//[**][%%]\n1**2%%3"));
	}

}
