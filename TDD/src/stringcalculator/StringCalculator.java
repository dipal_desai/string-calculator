package stringcalculator;

import java.security.InvalidParameterException;
import java.util.Arrays;

public class StringCalculator {

	public int add(String numbersToAdd) {
		
		StringBuilder pattern = new StringBuilder("\\n,");
		StringBuilder numbers = new StringBuilder();
		
		if(numbersToAdd.startsWith("//")) {
			pattern.append(numbersToAdd, numbersToAdd.indexOf("//"), numbersToAdd.indexOf("\n"));
			numbers.append(numbersToAdd.substring(numbersToAdd.indexOf("\n")).trim());
		}else {
			numbers.append(numbersToAdd);
		}
		
		return Arrays.stream(numbers.toString().split("["+ pattern +"]"))
				.map(String::strip)
				.filter(number -> !number.isBlank())
				.mapToInt(Integer::parseInt)
				.filter(number -> number < 1001)
				.peek(number -> {
					if(number < 0) {
						throw new InvalidParameterException("Negatives not allowed" + number);
					}
				})
				.sum();
	}
	
}
