**This project includes String calculator TDD kata**

Kata link: https://osherove.com/tdd-kata-1

What is TDD?

- Test Driven Development (TDD) is software development approach in which test cases are developed to specify and validate what the code will do

Basic rules of TDD?

- Write production code only to pass a failing unit test.
- Write no more of a unit test than sufficient to fail (compilation failures are failures).
- Write no more production code than necessary to pass the one failing unit test.